import java.util.Scanner;


public class FilePath {
    private  String inputFilePath ;
    private  String outputFilePath;
    private  String deleteFilePath;

    public String getInputFilePath() {
        return inputFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setInputFilePath(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }


    public String  inputFilePath()
    {

        System.out.println("Enter path along with name of file you want to create : ");
        Scanner inputFileLocation = new Scanner(System.in);
        inputFilePath = inputFileLocation.next();
        return inputFilePath;

    }

    public String outputFilePath()
    {

        System.out.println("Enter path along with name of file you want to read : ");
        Scanner outputFileLocation = new Scanner(System.in);
        outputFilePath = outputFileLocation.next();
        return outputFilePath;

    }

    public String deleteFilePath()
    {

        System.out.println("Enter path along with name of file you want to delete : ");
        Scanner deleteFileLocation = new Scanner(System.in);
        deleteFilePath = deleteFileLocation.next();
        return deleteFilePath;

    }

}
