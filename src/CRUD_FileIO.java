import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.Scanner;

public class CRUD_FileIO extends  FilePath {

    //private String filePath;
    private String data;
    private boolean check;

    public CRUD_FileIO(){
        //filePath = null;
        data  = null;
        check = Boolean.parseBoolean(null);
    }


    public String getData(){
        return data;
    }

    public void setData(String data){
        this.data = data;
    }

    public void creatingFile() throws IOException {

        File file = new File(inputFilePath());
        boolean check = file.createNewFile();

        if (check == true){
            System.out.println("File is created!");
        }else{
            System.out.println("File already exists.");
        }
    }



    public void reading() throws IOException{

        try {
            FileReader fileReader = new FileReader(outputFilePath());
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            while((line = bufferedReader.readLine())!= null)
            {
                System.out.println(line);
            }

            bufferedReader.close();

        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }

    }



    public void writingFile() throws IOException {
        System.out.println(" please enter the data to be written to file ");

        try {
        Scanner dataInput = new Scanner(System.in);

        data = dataInput.nextLine();

            FileWriter fileWriter = new FileWriter(outputFilePath());

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(data);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void appendFile() throws IOException {
//
        //add data to
        System.out.println("Add the additional data : ");
        Scanner addData = new Scanner(System.in);
        String additionalData = addData.nextLine();

        FileWriter fileWriter1 = new FileWriter(inputFilePath(), true);
        BufferedWriter bufferedWriter1 = new BufferedWriter(fileWriter1);

        PrintWriter pw = new PrintWriter(bufferedWriter1);
        pw.println(additionalData);
        pw.close();

        System.out.println("Data successfully appended at the end of file");
    }

        public void deleteFile () throws IOException {;

            try {
                File file =new  File(deleteFilePath());
                file.delete();
                setOutputFilePath("File Deleted Successfully ");

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
}
