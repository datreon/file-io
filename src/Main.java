import java.io.IOException;


public class Main {
    public static void main(String[] args) throws IOException {



        CRUD_FileIO CRUDFileIO = new CRUD_FileIO();

        CRUDFileIO.creatingFile();

        CRUDFileIO.inputFilePath();


        CRUDFileIO.writingFile();
        CRUDFileIO.reading();

        CRUDFileIO.appendFile();

        //comment this line to avoid deletion after creation
        CRUDFileIO.deleteFile();

//        CRUDFileIO.setData("File IO ");
//        CRUDFileIO.setFilePath("/home/datreon/sample.txt");

    }
}
